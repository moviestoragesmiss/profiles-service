﻿using System.Net;

namespace MovieStorage.Profiles.Responses;

public class ApiCallResponseBase
{
    public string Title;

    public string Description;
    
    public HttpStatusCode StatusCode;

}