﻿using MovieStorage.Profiles.Data;

namespace MovieStorage.Profiles.Responses;

public class DeleteProfileResponse : ApiCallResponseBase
{
    public Data.Profile DeletedProfile;
}