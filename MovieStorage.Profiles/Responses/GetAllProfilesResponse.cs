﻿using MovieStorage.Profiles.Data;

namespace MovieStorage.Profiles.Responses;

public class GetAllProfilesResponse : ApiCallResponseBase
{
    public List<Data.Profile> Profiles;
}