﻿using MovieStorage.Profiles.Data;

namespace MovieStorage.Profiles.Responses;

public class GetProfileResponse : ApiCallResponseBase
{
    public Data.Profile Profile;
}