﻿using MediatR;
using MovieStorage.Profiles.Commands;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Handlers.Commands;

public class DeleteProfileCommandHandler : IRequestHandler<DeleteProfileCommand, DeleteProfileResponse>
{
    public Task<DeleteProfileResponse> Handle(DeleteProfileCommand request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}