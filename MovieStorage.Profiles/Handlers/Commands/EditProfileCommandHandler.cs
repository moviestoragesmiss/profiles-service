﻿using MediatR;
using MovieStorage.Profiles.Commands;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Handlers.Commands;

public class EditProfileCommandHandler : IRequestHandler<EditProfileCommand, EditProfileResponse>
{
    public Task<EditProfileResponse> Handle(EditProfileCommand request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}