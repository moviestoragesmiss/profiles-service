﻿using MediatR;
using MongoDB.Driver;
using MovieStorage.Profiles.Data;
using MovieStorage.Profiles.Queries;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Handlers.Queries;

public class GetAllProfilesQueryHandler : IRequestHandler<GetAllProfilesQuery, GetAllProfilesResponse>
{
    private readonly MongoClient MongoClient;


    public GetAllProfilesQueryHandler(MongoClient mongoClient)
    {
        MongoClient = mongoClient;
    }

    public async Task<GetAllProfilesResponse> Handle(GetAllProfilesQuery request, CancellationToken cancellationToken)
    {
        var db = MongoClient.GetDatabase("ProfilesDatabase");
        var profiles = db.GetCollection<Data.Profile>("Profiles");
        var result = new GetAllProfilesResponse()
        {
            Profiles = await profiles.Aggregate()
                .ToListAsync(cancellationToken: cancellationToken)
        };
        return result;
    }
}