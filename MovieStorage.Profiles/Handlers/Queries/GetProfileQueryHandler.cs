﻿using MediatR;
using MongoDB.Driver;
using MovieStorage.Profiles.Queries;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Handlers.Queries;

public class GetProfileQueryHandler : IRequestHandler<GetProfileQuery, GetProfileResponse>
{
    private readonly MongoClient MongoClient;

    public GetProfileQueryHandler(MongoClient mongoClient)
    {
        MongoClient = mongoClient;
    }

    public Task<GetProfileResponse> Handle(GetProfileQuery request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}