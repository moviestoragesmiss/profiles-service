﻿using MediatR;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Queries;

public class GetAllProfilesQuery : IRequest<GetAllProfilesResponse>
{
    
}