﻿using MediatR;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Queries;

public class GetProfileQuery : IRequest<GetProfileResponse>
{
    
}