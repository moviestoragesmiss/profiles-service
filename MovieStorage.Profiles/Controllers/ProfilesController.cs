﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieStorage.Profiles.Commands;
using MovieStorage.Profiles.Data;
using MovieStorage.Profiles.Filters.ActionFilters;
using MovieStorage.Profiles.Filters.ErrorFilters;
using MovieStorage.Profiles.Queries;
using Profile;

namespace MovieStorage.Profiles.Controllers;

[ApiController]
[Route("[controller]")]
public class ProfilesController : ControllerBase
{
    private readonly IMediator  _mediator;
    
    public ProfilesController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet]
    [GetAllProfilesActionFilter]
    [GetAllProfilesErrorFilter]
    [Route("~/getAll")]
    public async Task<IActionResult> GetAllProfiles()
    {
        var command = new GetAllProfilesQuery();
        var result = await _mediator.Send(command);
        return Ok(result);
    }
    
    [HttpGet]
    [GetProfileActionFilter]
    [GetProfileErrorFilter]
    [Route("~/getProfile")]
    public async Task<IActionResult> GetProfile([FromBody]string id)
    {
        var command = new GetProfileQuery();
        var result = await _mediator.Send(command);
        return Ok(result);
    }
    
    [HttpDelete]
    [DeleteProfileActionFilter]
    [DeleteProfileErrorFilter]
    [Route("~/deleteProfile")]
    public async Task<IActionResult> DeleteProfile([FromBody]string id)
    {
        var command = new DeleteProfileCommand();
        var result = await _mediator.Send(command);
        return Ok(result);
    }
    
    [HttpPatch]
    [EditProfileActionFilter]
    [EditProfileErrorFilter]
    [Route("~/editProfile")]
    public async Task<IActionResult> EditProfile([FromBody]ProfileDTO profile)
    {
        var command = new EditProfileCommand();
        var result = await _mediator.Send(command);
        return Ok(result);
    }
}