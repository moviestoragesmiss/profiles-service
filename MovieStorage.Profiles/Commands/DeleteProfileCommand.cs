﻿using MediatR;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Commands;

public class DeleteProfileCommand : IRequest<DeleteProfileResponse>
{
    
}