﻿using MediatR;
using MovieStorage.Profiles.Responses;

namespace MovieStorage.Profiles.Commands;

public class EditProfileCommand : IRequest<EditProfileResponse>
{
    
}