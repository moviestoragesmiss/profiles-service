﻿using System.Reflection;
using MassTransit;
using MongoDB.Driver;
using MovieStorage.Profiles.BusConsumer;
using Serilog;

namespace MovieStorage.Profiles;

public static class HostingExtensions
{
    public static WebApplication ConfigureServices(this WebApplicationBuilder builder)
    {
        var configuration = builder.Configuration;
        var mongoDbConnectionString = configuration.GetConnectionString("MongoDbConnectionString");
        builder.Services.AddControllers();
        builder.Services.AddSingleton(new MongoClient(mongoDbConnectionString));
        builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
        builder.Services.AddMassTransit(x =>
        {
            x.AddConsumer<ProfileConsumerNotification>();
            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("rmuser");
                    h.Password("rmpassword");
                });
                cfg.ConfigureEndpoints(context);
            });
        });
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        return builder.Build();
    }
    
    public static WebApplication ConfigurePipeline(this WebApplication app)
    {
        app.UseSerilogRequestLogging();
        
        app.UseAuthorization();
        app.UseAuthentication();
        app.UseHttpsRedirection();
        app.MapControllers();
        app.UseSwagger();
        app.UseSwaggerUI();
        
        if (!app.Environment.IsDevelopment()) return app;
        app.UseDeveloperExceptionPage();
        return app;
    }

}