﻿using System.Text.Json;
using MassTransit;
using MovieStorage.Profiles.Data;
using Profile;

namespace MovieStorage.Profiles.BusConsumer;

public class ProfileConsumerNotification : IConsumer<ProfileDTO>
{
    public async Task Consume(ConsumeContext<ProfileDTO> context)
    {
        var serializedMessage = JsonSerializer.Serialize(context.Message, new JsonSerializerOptions { });
        Console.WriteLine($"NotificationCreated event consumed. Message: {serializedMessage}");
    }
}